import http from "k6/http";
import { check, sleep } from "k6";
import { parseHTML } from "k6/html";

const debug = true;

export const options = {
  stages: [
    { duration: "10s", target: 100 },
    { duration: "5s", target: 200 },
    { duration: "5s", target: 300 },
    { duration: "5s", target: 400 },
    { duration: "5s", target: 500 },
    { duration: "10s", target: 0 },
  ],
  thresholds: {
    http_req_duration: ["p(99) <= 60000"],
  },
};

export default function () {
  const BASE_URL = "https://litnow.vn/";
  const responses = http.get(BASE_URL);

  if (debug) {
    const doc = parseHTML(responses.body);
    const pageTitle = doc.find("head title").text();
    console.log(pageTitle);
  }

  check(responses, {
    "all status 200": (r) => r.status == 200,
  });

  sleep(1);
}
