import http from "k6/http";
import { check, sleep } from "k6";

export const ACCOUNTS = [
  {
    phoneNum: "0903733091",
    pwd: "12345678x@X",
  },
  {
    phoneNum: "0987654321",
    pwd: "123456kk",
  },
  {
    phoneNum: "0937942974",
    pwd: "2512@phu",
  },
  {
    phoneNum: "0902389820",
    pwd: "12345678x@X",
  },
];

export const options = {
  stages: [
    { duration: "10s", target: 100 },
    { duration: "5s", target: 200 },
    { duration: "5s", target: 300 },
    { duration: "5s", target: 400 },
    { duration: "5s", target: 500 },
    { duration: "10s", target: 0 },
  ],
  thresholds: {
    http_req_duration: ["p(99) <= 60000"],
  },
};

export default function () {
  const BASE_URL = "https://litnow.geekdana.com";
  const METHOD = "POST";
  const URL = `${BASE_URL}/litnow/api/v1/mobile/login`;
  const PARAMS = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const batchRequests = ACCOUNTS.map((account) => [
    METHOD,
    URL,
    JSON.stringify({
      phoneNum: account.phoneNum,
      pwd: account.pwd,
      languageType: "vi",
    }),
    PARAMS,
  ]);

  const responses = http.batch(batchRequests);

  // "all success login:": (r) => JSON.parse(responses[0].body).result.userId == "2000034",
  check(responses[0], {
    "all status 200": (r) => r.status == 200,
  });
  check(responses[1], {
    "all status 200": (r) => r.status == 200,
  });
  check(responses[2], {
    "all status 200": (r) => r.status == 200,
  });
  check(responses[3], {
    "all status 200": (r) => r.status == 200,
  });

  // sleep(1);
}
